package com.diolk49.schedule.models;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.SystemClock;
import android.provider.CalendarContract;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import com.diolk49.schedule.MainActivity;
import com.diolk49.schedule.receivers.TaskActivityReceiver;
import com.diolk49.schedule.ui.task.TaskFragment;
import com.diolk49.schedule.ui.task.ViewTaskActivity;

import java.net.URI;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class Task {
    private Long date;
    private String name;
    private int duration;
    private String state;
    private String recurrence;
    private int numberOfReminder;
    private String uid_parent;
    private String key;
    private String event_id;


    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public Task() {
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUid_parent() {
        return uid_parent;
    }

    public void setUid_parent(String uid_parent) {
        this.uid_parent = uid_parent;
    }

    public Task(String name, int duration, String state, String recurrence, int numberOfReminder) {
        this.name = name;
        this.duration = duration;
        this.state = state;
        this.recurrence = recurrence;
        this.numberOfReminder = numberOfReminder;
    }

    private final ArrayList<String> task_states = new ArrayList<String>() {{
        add("todo");
        add("done");
    }};

    private final ArrayList<String> task_recurrence = new ArrayList<String>() {{
        add("never");
        add("hourly");
        add("daily");
        add("weekly");
        add("monthly");
    }};

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getRecurrence() {
        return recurrence;
    }

    public void setRecurrence(String recurrence) throws Exception {
        if (task_recurrence.contains(recurrence)) {
            this.recurrence = recurrence;
        } else {
            throw new Exception("Recurrence must be one of ['never', 'hourly', 'daily', 'weekly', 'monthly']");
        }
    }

    public int getNumberOfReminder() {
        return numberOfReminder;
    }

    public void setNumberOfReminder(int numberOfReminder) {
        this.numberOfReminder = numberOfReminder;
    }

    public String getState() {
        return state;
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void addNotification(Activity activity){
        Calendar calendar = Calendar.getInstance();
        Calendar today_calendar = Calendar.getInstance();

        // Task is a one time task and has been done. No need for an alarm.
        if (this.getState() == "done" && this.getRecurrence() == "never"){
            Log.d("task.java//addNotifi...", "Task is done. We won't create alarm for : " + this.getName());
            return;
        }
        // Task is a one time task and is too old. No need for an alarm.
        if (this.getDate() < calendar.getTimeInMillis() && this.getRecurrence() == "never"){
            Log.d("task.java//addNotifi...", "Task is too old. We won't create alarm for : " + this.getName());
            return;
        }

        AlarmManager alarmMgr;

        alarmMgr = (AlarmManager)activity.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(activity.getApplication(), TaskActivityReceiver.class);
        intent.putExtra("task_key", this.getKey());
        intent.putExtra("task_name", this.getName());
        PendingIntent alarmIntent = PendingIntent.getBroadcast(activity.getApplication(), 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);


        calendar.setTimeInMillis(this.getDate());
        // We set the day as today's day.
        calendar.set(Calendar.DAY_OF_YEAR, today_calendar.get(Calendar.DAY_OF_YEAR));

        // Set the alarm to start at approximately 15 min before the task date.
        calendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE) - 15);
//        alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_FIFTEEN_MINUTES, alarmIntent);

        setTaskNotificationRecurrence(alarmMgr, calendar, alarmIntent);

        switch (this.getNumberOfReminder()){
            case 2:
                 // Set the alarm to start at approximately 1 hour before the task date.
                calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) - 1);
                setTaskNotificationRecurrence(alarmMgr, calendar, alarmIntent);
                break;
            case 3:
                // Set the alarm to start at approximately 1 hour before the task date.
                calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) - 1);
                setTaskNotificationRecurrence(alarmMgr, calendar, alarmIntent);

                // Set the alarm to start at approximately 2 hours before the task date.
                calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) - 2);
                setTaskNotificationRecurrence(alarmMgr, calendar, alarmIntent);
                break;
            case 4:
                // Set the alarm to start at approximately 1 hour before the task date.
                calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) - 1);
                setTaskNotificationRecurrence(alarmMgr, calendar, alarmIntent);


                // Set the alarm to start at approximately 2 hours before the task date.
                calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) - 2);
                setTaskNotificationRecurrence(alarmMgr, calendar, alarmIntent);

                // Set the alarm to start at approximately 3 hours before the task date.
                calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) - 3);
                setTaskNotificationRecurrence(alarmMgr, calendar, alarmIntent);

                break;
            default:
                break;
        }


        Log.d("task.java//addNotifi...", "Add of a notification for this task : " + this.getName());
    }

    public String getEvent(Activity activity) {
//        // Run query
//        Cursor cur = null;
//        ContentResolver cr = activity.getContentResolver();
//        Uri uri = CalendarContract.Calendars.CONTENT_URI;
//        String selection = "((" + CalendarContract.Calendars.ACCOUNT_NAME + " = ?) AND ("
//                + CalendarContract.Calendars.ACCOUNT_TYPE + " = ?) AND ("
//                + CalendarContract.Calendars.OWNER_ACCOUNT + " = ?))";
//        String[] selectionArgs = new String[] {"hera@example.com", "com.example",
//                "hera@example.com"};
//// Submit the query and get a Cursor object back.
//        cur = cr.query(uri, EVENT_PROJECTION, selection, selectionArgs, null);

        return this.name;

    }
    public String addToCalendar(Activity activity) {
        long calID = 3;
        long startMillis = 0;
        long endMillis = 0;
        Calendar cal = Calendar.getInstance();

        Calendar beginTime = Calendar.getInstance();
        beginTime.set(2020, 4, 15, 18, 5);
        startMillis = beginTime.getTimeInMillis();
        Calendar endTime = Calendar.getInstance();
        endTime.set(2020, 4, 15, 23, 45);
        endMillis = endTime.getTimeInMillis();


        ContentResolver cr = activity.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.DTSTART, startMillis);
        values.put(CalendarContract.Events.DTEND, endMillis);
        values.put(CalendarContract.Events.TITLE, this.getName());

        values.put(CalendarContract.Events.DESCRIPTION, this.getUid_parent());
        values.put(CalendarContract.Events.CALENDAR_ID, calID);
        String timeZone = TimeZone.getDefault().getID();
        values.put(CalendarContract.Events.EVENT_TIMEZONE, timeZone);
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.WRITE_CALENDAR}, 0);

        }
        Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);

        long eventID = Long.parseLong(uri.getLastPathSegment());
/*
        ContentResolver cr_2 = activity.getContentResolver();
        ContentValues values_2 = new ContentValues();
        Uri updateUri = null;

// The new title for the event
        values.put(CalendarContract.Events.TITLE, "Kickboxing");
        updateUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, eventID);
        int rows = cr.update(updateUri, values, null, null);
*/

        ContentValues reminders = new ContentValues();

        reminders.put(CalendarContract.Reminders.MINUTES, 1);
        reminders.put(CalendarContract.Reminders.EVENT_ID, eventID);
        reminders.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_SMS);

        activity.getContentResolver().insert(CalendarContract.Reminders.CONTENT_URI, reminders);

        //getCalendarId(activity.getApplicationContext(), activity);
        return String.valueOf(eventID);
    }
    private Long getCalendarId(Context context, Activity activity) {
        String[] projection = new String[]{
                CalendarContract.Calendars._ID,
                CalendarContract.Calendars.CALENDAR_DISPLAY_NAME};

        Cursor cur = null;
        ContentResolver cr = context.getContentResolver();
        Uri uri = CalendarContract.Calendars.CONTENT_URI;
        String selection = "((" + CalendarContract.Calendars.ACCOUNT_NAME + " = ?) AND ("
                + CalendarContract.Calendars.ACCOUNT_TYPE + " = ?) AND ("
                + CalendarContract.Calendars.OWNER_ACCOUNT + " = ?))";
        String[] selectionArgs = new String[]{"louison.diogo@gmail.com"};

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.READ_CALENDAR}, 1);
        }
        cur = cr.query(uri, projection, selection, selectionArgs, null);


        Cursor calCursor = context.getContentResolver().query(
                CalendarContract.Calendars.CONTENT_URI,
                projection,
                CalendarContract.Calendars.VISIBLE + " = 1 AND " + CalendarContract.Calendars.IS_PRIMARY + "=1",
                null,
                CalendarContract.Calendars._ID + " ASC"
        );

/*        Cursor calCursor = new Cursor();
        if (calCursor != null && calCursor.getCount() >= 0) {*/
//            Cursor calCursor = context.getContentResolver().query(
//                    CalendarContract.Calendars.CONTENT_URI,
//                    projection,
//                    CalendarContract.Calendars.VISIBLE + " = 1",
//                    null,
//                    CalendarContract.Calendars._ID + " ASC"
//            );
//        }

        if (calCursor != null) {
            if (calCursor.moveToFirst()) {
                int nameCol = calCursor.getColumnIndex(projection[1]);
                int idCol = calCursor.getColumnIndex(projection[0]);

                String calName = calCursor.getString(nameCol);
                String calID = calCursor.getString(idCol);

                Log.d("1", "Calendar name = $calName Calendar ID = $calID");

                calCursor.close();
                return Long.parseLong(String.valueOf(calID));
            }
        }
        return null;
    }

    public void setState(String state) throws Exception {
        if (task_states.contains(state)){
            this.state = state;
        }
        else{
            throw new Exception("State must be one of ['todo', 'done']");
        }
    }

    public static String GetTaskStateFromPretty(String pretty_text){
        switch (pretty_text){
            case "à faire":
                return "todo";
            case "fait":
                return "done";
            default:
                return "todo";
        }
    }

    public static String PrettifyTaskState(String text){
        switch (text){
            case "todo":
                return "à faire";
            case "done":
                return "fait";
            default:
                return "à faire";
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public String PrettifyDate() {
        Instant date_instant = Instant.ofEpochMilli(this.date);

        if (this.getRecurrence().compareTo("daily") == 0){
            String hour = String.valueOf(date_instant.atZone(ZoneId.systemDefault()).getHour())
                    + ":" + String.valueOf(date_instant.atZone(ZoneId.systemDefault()).getMinute());

            return hour;
        }
        else{
            DateTimeFormatter formatter =
                    DateTimeFormatter.ofLocalizedDateTime( FormatStyle.SHORT )
                            .withLocale( Locale.FRANCE )
                            .withZone( ZoneId.systemDefault() );
            return formatter.format(date_instant);
        }

    }

    public static String GetTaskRecurrenceFromPretty(String pretty_text){
//        Recurrence must be one of ['never', 'hourly', 'daily', 'weekly', 'monthly']

        switch (pretty_text){
            case "1 fois":
                return "never";
            case "Toutes les heures":
                return "hourly";
            case "Tous les jours":
                return "daily";
            case "Toutes les semaines":
                return "weekly";
            case"Tous les mois":
                return "monthly";
            default:
                return "never";
        }
    }

    public static String PrettifyRecurrence(String text){
//        Recurrence must be one of ['never', 'hourly', 'daily', 'weekly', 'monthly']

        switch (text){
            case "never":
                return "1 fois";
            case "hourly":
                return "Toutes les heures";
            case "daily":
                return "Tous les jours";
            case "weekly":
                return "Toutes les semaines";
            case"monthly":
                return "Tous les mois";
            default:
                return "1 fois";
        }
    }

    private void setTaskNotificationRecurrence(AlarmManager alarmMgr, Calendar calendar, PendingIntent alarmIntent){
        switch(this.getRecurrence()){
            case "hourly":
                alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_HOUR, alarmIntent);
                break;
            case "daily":
                alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, alarmIntent);
                break;
            case "weekly":
                alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY * 7, alarmIntent);
                break;
            case "monthly":
                alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY * 30, alarmIntent);
                break;
            default:
                alarmMgr.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);
                break;
        }
    }
}
