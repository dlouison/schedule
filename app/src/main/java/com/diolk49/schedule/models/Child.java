package com.diolk49.schedule.models;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class Child extends Profile {
    private String uid_parent;

    public String getUid_parent() {
        return uid_parent;
    }

    public void setUid_parent(String uid_parent) {
        this.uid_parent = uid_parent;
    }

    public Child(String fullname, String uid_parent) {
        super("", fullname);
        this.uid_parent = uid_parent;
    }

    public static Child GetFromPreferences(Context appContext){
        SharedPreferences sharedPref = appContext.getSharedPreferences(
                "com.diolk49.schedule.child", Context.MODE_PRIVATE);

        try{
            Child child = new Child(sharedPref.getString("fullname", ""), sharedPref.getString("uid_parent", ""));
            return child;
        } catch (Exception e) {
            return null;
        }
    }

    public void SavePreferences(Context appContext, Activity activity){

        SharedPreferences sharedPref = appContext.getSharedPreferences(
                "com.diolk49.schedule.child", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("fullname", this.getFullname());
        editor.putString("uid_parent", this.getUid_parent());
        editor.commit();
    }
}
