package com.diolk49.schedule.models;

import com.google.firebase.database.DatabaseReference;

public class Parent extends Profile {
    private String uid;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Parent() { }

    public Parent(String email, String fullname, String uid) {
        super(email, fullname);
        this.uid = uid;
    }

    public void Save_to_db(DatabaseReference mDatabase){
        mDatabase.child("parents").push().setValue(this);
    }
}
