package com.diolk49.schedule.ui.task;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModelProviders;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.icu.text.LocaleDisplayNames;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.diolk49.schedule.R;
import com.diolk49.schedule.models.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class TaskFragment extends Fragment {

    private TaskViewModel mViewModel;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;

    private FirebaseUser currentUser;

    private Spinner task_recurrence;

    private EditText name;
    private NumberPicker duration;
    private NumberPicker reminder;
    private Button save_task;

    private EditText date;
    private EditText time;
//    private DatePicker date;

    public static TaskFragment newInstance() {
        return new TaskFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.task_fragment, container, false);


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(TaskViewModel.class);
        mAuth = FirebaseAuth.getInstance();

        currentUser = mAuth.getCurrentUser();

        task_recurrence = (Spinner) getView().findViewById(R.id.task_recurrence);
        name = getView().findViewById(R.id.task_name);
        duration = getView().findViewById(R.id.task_duration);
        reminder = getView().findViewById(R.id.task_reminders);
        date = getView().findViewById(R.id.task_date);
        time = getView().findViewById(R.id.task_time);

        reminder.setMinValue(0);
        reminder.setMaxValue(10);
        duration.setMinValue(1);
        duration.setMaxValue(1440);

        save_task = getView().findViewById(R.id.save_task);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getView().getContext(),
                R.array.task_recurrence, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        task_recurrence.setAdapter(adapter);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("tasks");

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Get Current Date
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                date.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, mYear, mMonth, mDay);

                datePickerDialog.show();
            }
        });
        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Get Current Time
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                time.setText(hourOfDay + ":" + minute);
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        save_task.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                Task task = new Task(
                        name.getText().toString(),
                        duration.getValue(),
                        "todo",
                        Task.GetTaskRecurrenceFromPretty(task_recurrence.getSelectedItem().toString()),
                        reminder.getValue()
                );

                task.setDate(getTaskDate());
                //task.setDate(getDateFromDatePicker(date));
                task.setUid_parent(currentUser.getUid());
                //String event_id = task.addToCalendar(getActivity());
                //task.setEvent_id(event_id);

                DatabaseReference db_ref = mDatabase.push();

                String task_key = db_ref.getKey();
                task.setKey(task_key);

                db_ref.setValue(task);

                task.addNotification(getActivity());

                Toast.makeText(getContext(), "Tâche créée.", Toast.LENGTH_SHORT).show();

            }


        });


    }
    public static java.util.Date getDateFromDatePicker(DatePicker datePicker){
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year =  datePicker.getYear();

        Calendar calendar;
        calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        return calendar.getTime();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private Long getTaskDate() {

        SimpleDateFormat task_date_formatter = new SimpleDateFormat("d/m/yyyyh:m");
        Date task_date = new Date();
        try {
            task_date = task_date_formatter.parse(date.getText().toString() + time.getText().toString());
        } catch (ParseException e) {
            Toast.makeText(getActivity(),"Veuillez saisir une date et une heure svp", Toast.LENGTH_SHORT).show();
        }


        return task_date.toInstant().toEpochMilli();
    }
}

