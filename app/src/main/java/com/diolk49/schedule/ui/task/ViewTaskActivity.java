package com.diolk49.schedule.ui.task;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.diolk49.schedule.MainActivity;
import com.diolk49.schedule.R;
import com.diolk49.schedule.models.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.time.Duration;

public class ViewTaskActivity extends AppCompatActivity {
    private DatabaseReference mDatabase;

    private TextView task_name;
    private TextView task_date;
    private TextView task_recurrence;
    private TextView task_duration;
    private TextView task_reminders;
    private TextView task_state;
    private Button delete_task;
    private Button mark_as_done;

    private Task task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_task);


        Intent intent = getIntent();
        String task_key = intent.getStringExtra("task_key");

        mDatabase = FirebaseDatabase.getInstance().getReference();

        task = new Task();

        task_name = findViewById(R.id.task_name);
        task_date = findViewById(R.id.task_date);
        task_recurrence = findViewById(R.id.task_recurrence);
        task_duration = findViewById(R.id.task_duration);
        task_reminders = findViewById(R.id.task_reminders);
        task_state = findViewById(R.id.task_state);

        delete_task = findViewById(R.id.delete_task);
        mark_as_done = findViewById(R.id.mark_as_done);

        Query tasks_query = mDatabase.child("tasks").child(task_key);
        tasks_query.addValueEventListener(get_listener());

        delete_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference db_ref = mDatabase.push();
                tasks_query.getRef().removeValue();

                Intent intent = new Intent(ViewTaskActivity.this, TaskListForChildActivity.class);
                finish();
                startActivity(intent);
            }
        });

        mark_as_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    task.setState("done");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                tasks_query.getRef().setValue(task);
            }
        });
    }

    private ValueEventListener get_listener(){
        return new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Task m_task = dataSnapshot.getValue(Task.class);

                task = m_task;

                try {
                    Duration duration = Duration.ofSeconds(task.getDuration());

                    task_name.setText(task.getName());
                    task_date.setText(task.PrettifyDate());
                    task_duration.setText(duration.toString());
                    task_recurrence.setText("Se repete : " + Task.PrettifyRecurrence(task.getRecurrence()));
                    task_reminders.setText(task.getNumberOfReminder() + " rappels");
                    task_state.setText(Task.PrettifyTaskState(task.getState()));
                }
                catch (Exception e){
                    Log.d("001 Retreiving tasks", "Error retreiving task, it may not exists anymore");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(ViewTaskActivity.this, "La tâche n'a pas pu être récupérée", Toast.LENGTH_SHORT).show();
            }

        };
    }

}
