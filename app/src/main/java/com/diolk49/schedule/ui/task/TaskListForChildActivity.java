
package com.diolk49.schedule.ui.task;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.diolk49.schedule.R;
import com.diolk49.schedule.adapters.TaskListAdapter;
import com.diolk49.schedule.models.Child;
import com.diolk49.schedule.models.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class TaskListForChildActivity extends AppCompatActivity {
    private Child child;
    private DatabaseReference mDatabase;

    private ArrayList<Task> tasks;

    private RecyclerView task_list;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_list_for_child);

        child = Child.GetFromPreferences(this);
        mDatabase = FirebaseDatabase.getInstance().getReference();

        tasks = new ArrayList<Task>();

        // Get tasks
        Query tasks_query = mDatabase.child("tasks").orderByChild("uid_parent").equalTo(child.getUid_parent());
        tasks_query.addValueEventListener(get_listener());

        //Toast.makeText(this, "L'enfant est : " + child.getFullname(), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();

        task_list = findViewById(R.id.task_list);
        layoutManager = new LinearLayoutManager(this);
        task_list.setLayoutManager(layoutManager);

        mAdapter = new TaskListAdapter(tasks);
        task_list.setAdapter(mAdapter);
    }

    private ValueEventListener get_listener() {
        ValueEventListener valuesListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                tasks.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Task task = postSnapshot.getValue(Task.class);
                    task.setKey(postSnapshot.getKey());
                    tasks.add(task);
                }

                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Toast.makeText(TaskListForChildActivity.this, "Failed to retrieve tasks", Toast.LENGTH_SHORT).show();
            }
        };

        return valuesListener;

    }
}
