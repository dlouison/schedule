package com.diolk49.schedule.ui.main;

import androidx.lifecycle.ViewModelProviders;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.diolk49.schedule.MainActivity;
import com.diolk49.schedule.R;
import com.diolk49.schedule.TaskActivity;
import com.diolk49.schedule.ui.LoginActivity;
import com.diolk49.schedule.ui.LoginChildActivity;
import com.diolk49.schedule.ui.SignupActivity;
import com.diolk49.schedule.ui.task.TaskListForChildActivity;
import com.google.firebase.auth.FirebaseAuth;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;
    private FirebaseAuth mAuth;

    private Button create_task;
    private Button login_button;
    private Button signup_button;
    private Button signup_button_child;
    private Button copy_uid;
    private Button show_tasks;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {


        return inflater.inflate(R.layout.main_fragment, container, false);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        mAuth = FirebaseAuth.getInstance();

        create_task = getActivity().findViewById(R.id.create_task);
        login_button = getActivity().findViewById(R.id.login);
        signup_button = getActivity().findViewById(R.id.signup);
        copy_uid = getActivity().findViewById(R.id.copy_uid);
        signup_button_child = getActivity().findViewById(R.id.set_uid);
        show_tasks = getActivity().findViewById(R.id.show_tasks);

        create_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent int_task = new Intent(getActivity(), TaskActivity.class);
                startActivity(int_task);
            }
        });

        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent int_task = new Intent(getActivity(), LoginActivity.class);
                startActivity(int_task);
            }
        });
        signup_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent int_task = new Intent(getActivity(), SignupActivity.class);
                startActivity(int_task);
            }
        });
        signup_button_child.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent int_task = new Intent(getActivity(), LoginChildActivity.class);
                startActivity(int_task);
            }
        });
        show_tasks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent int_task = new Intent(getActivity(), TaskListForChildActivity.class);
                startActivity(int_task);
            }
        });

        copy_uid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAuth.getCurrentUser() != null){
                    ClipboardManager clipboard = (ClipboardManager)
                            getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("UID parent", mAuth.getCurrentUser().getUid());
                    clipboard.setPrimaryClip(clip);
                }
                Toast.makeText(getContext(), "UID " + mAuth.getCurrentUser().getUid() + " copié !", Toast.LENGTH_SHORT).show();
            }
        });


    }

}
