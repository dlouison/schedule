package com.diolk49.schedule.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.diolk49.schedule.MainActivity;
import com.diolk49.schedule.R;
import com.diolk49.schedule.models.Child;
import com.diolk49.schedule.ui.task.TaskListForChildActivity;

public class LoginChildActivity extends AppCompatActivity {

    private Button login_button;
    private EditText fullname_et;
    private EditText uid_parent_et;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_child);

        Child child = Child.GetFromPreferences(this.getApplicationContext());

        login_button = findViewById(R.id.signup_child);
        fullname_et = findViewById(R.id.fullname_et);
        uid_parent_et = findViewById(R.id.ui_parent_et);

        if(child != null && child.getFullname() != ""){
            fullname_et.setText(child.getFullname());
            uid_parent_et.setText(child.getUid_parent());
        }
        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Child child = new Child(fullname_et.getText().toString(), uid_parent_et.getText().toString());

                child.SavePreferences(LoginChildActivity.this, LoginChildActivity.this);

                Intent int_task = new Intent(LoginChildActivity.this, TaskListForChildActivity.class);
                finish();
                startActivity(int_task);
            }
        });


    }
}
