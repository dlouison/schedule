package com.diolk49.schedule.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.diolk49.schedule.MainActivity;
import com.diolk49.schedule.R;
import com.diolk49.schedule.models.Parent;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignupActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser = null;
    private DatabaseReference mDatabase;

    private Button signup_button;
    private EditText email_et;
    private EditText password_et;
    private EditText fullname_et;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        signup_button = findViewById(R.id.signup);
        email_et = findViewById(R.id.email_et);
        password_et = findViewById(R.id.password_et);
        fullname_et = findViewById(R.id.fullname_et);

        signup_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = email_et.getText().toString();
                String password = password_et.getText().toString();
                String fullname = fullname_et.getText().toString();

                if(currentUser == null){
                    create_user(email, password);
                }
                else{
                    Toast.makeText(SignupActivity.this, "Vous êtes déjà connecté", Toast.LENGTH_SHORT).show();

                    Intent int_task = new Intent(SignupActivity.this, MainActivity.class);
                    finish();
                    startActivity(int_task);
                }
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();

        // Check if user is signed in (non-null) and update UI accordingly.
        currentUser = mAuth.getInstance().getCurrentUser();

        if (currentUser != null){
            mAuth.signOut();
            Toast.makeText(this, "Vous êtes déconnecté", Toast.LENGTH_SHORT).show();
            currentUser = mAuth.getInstance().getCurrentUser();
        }
    }

    private void create_user(String email, String password){
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            currentUser = mAuth.getCurrentUser();

                            Parent parent = new Parent(currentUser.getEmail(), fullname_et.getText().toString(), currentUser.getUid());

                            parent.Save_to_db(mDatabase);

                            Toast.makeText(SignupActivity.this, "Profil parent créé pour " + parent.getFullname(), Toast.LENGTH_SHORT).show();

                            Intent int_task = new Intent(SignupActivity.this, MainActivity.class);
                            finish();
                            startActivity(int_task);
                        }
                        else{
                            Toast.makeText(SignupActivity.this, "Impossible de créer le compte.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

}
