package com.diolk49.schedule.adapters;

import android.animation.LayoutTransition;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.diolk49.schedule.R;
import com.diolk49.schedule.models.Task;
import com.diolk49.schedule.ui.task.TaskListForChildActivity;
import com.diolk49.schedule.ui.task.ViewTaskActivity;

import java.util.ArrayList;
import java.util.List;

import static androidx.core.content.ContextCompat.startActivity;

public class TaskListAdapter extends RecyclerView.Adapter<TaskListAdapter.TaskListViewHolder> {
    private ArrayList<Task> task_list;
    private Task current_task;

    public TaskListAdapter(ArrayList<Task> m_task_list){
        task_list = m_task_list;
    }

    @NonNull
    @Override
    public TaskListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.task_list_item, parent, false);

        current_task = new Task();

        return new TaskListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TaskListViewHolder holder, int position){

        Task task = task_list.get(position);
        current_task = task;
        holder.display(task);
    }

    @Override
    public int getItemCount() {
        return task_list.size();
    }

    public class TaskListViewHolder extends RecyclerView.ViewHolder{
        private final TextView task_name;
        private final TextView task_key;

        public TaskListViewHolder(@NonNull final View itemView) {
            super(itemView);

            task_name = itemView.findViewById(R.id.task_name);
            task_key = itemView.findViewById(R.id.task_key);

            itemView.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View v) {
                    Intent int_task = new Intent(itemView.getContext(), ViewTaskActivity.class);
                    int_task.putExtra("task_key", task_key.getText().toString());
                    itemView.getContext().startActivity(int_task);
                    Toast.makeText(itemView.getContext(), "Tâche cliquée " + task_name.getText().toString(), Toast.LENGTH_SHORT).show();
                }
            });
        }

        public void display(Task task){
            task_name.setText("Tache : " + task.getName() + " "+ Task.PrettifyTaskState(task.getState()) );
            task_key.setText(task.getKey());
        }
    }
}

