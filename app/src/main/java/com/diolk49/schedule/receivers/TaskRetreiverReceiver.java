package com.diolk49.schedule.receivers;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.diolk49.schedule.MainActivity;
import com.diolk49.schedule.R;
import com.diolk49.schedule.models.Child;
import com.diolk49.schedule.models.Task;
import com.diolk49.schedule.ui.task.TaskListForChildActivity;
import com.diolk49.schedule.ui.task.ViewTaskActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class TaskRetreiverReceiver extends BroadcastReceiver {
    private Child child;
    private DatabaseReference mDatabase;

    private ArrayList<Task> tasks;

    @Override
    public void onReceive(Context context, Intent intent) {

        child = Child.GetFromPreferences(context);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        tasks = new ArrayList<Task>();

        Query tasks_query = mDatabase.child("tasks").orderByChild("uid_parent").equalTo(child.getUid_parent());
        tasks_query.addValueEventListener(get_listener(MainActivity.activity));
    }

    private ValueEventListener get_listener(Activity activity) {
        ValueEventListener valuesListener = new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                tasks.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Task task = postSnapshot.getValue(Task.class);
                    task.setKey(postSnapshot.getKey());

                    task.addNotification(activity);
                    tasks.add(task);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
            }
        };

        return valuesListener;

        }

}
