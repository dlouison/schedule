package com.diolk49.schedule;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.diolk49.schedule.ui.LoginActivity;
import com.diolk49.schedule.ui.task.TaskFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class TaskActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;

    protected FirebaseUser currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mAuth = FirebaseAuth.getInstance();

        super.onCreate(savedInstanceState);
        
        if (mAuth.getCurrentUser() == null){
            Toast.makeText(this, "Vous devez être connecté", Toast.LENGTH_SHORT).show();
            Intent int_task = new Intent(TaskActivity.this, LoginActivity.class);
            startActivity(int_task);
        }
        else{
            currentUser = mAuth.getCurrentUser();
        }

        setContentView(R.layout.task_activity);


        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, TaskFragment.newInstance())
                    .commitNow();
        }


    }

}
